﻿using FindMyIdentity.Lib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FindMyIdentityTest.Lib
{
    public class UrlToolsTest
    {
        [Fact]
        public void TestExtractQueryParam()
        {
            Assert.Equal("http://localhost:3000/callback", UrlTools.ExtractQueryParam(Path, "RedirectUrl"));
            Assert.Equal("5fd772d0-5bc2-4e54-812c-f745803f248c", UrlTools.ExtractQueryParam(Path, "key"));
        }

        private const string Path = "/Token/IdToken?RedirectUrl=http://localhost:3000/callback&key=5fd772d0-5bc2-4e54-812c-f745803f248c";
    }
}
