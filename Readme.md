
CREATE USER findmyidentity WITH PASSWORD 'findmyidentity';

CREATE DATABASE findmyidentity;
GRANT ALL PRIVILEGES ON DATABASE findmyidentity to findmyidentity;

\c  findmyidentity

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public; -- Run as super user for each database
