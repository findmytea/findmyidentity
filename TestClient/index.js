const express = require('express')
const axios = require('axios');
const jwt = require('jwt-decode');
const app = express()
const port = 3000

const key = 'cf174eee-31ac-4425-8f8e-fbd90b2d1696';

const HEADERS = {
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
};

app.get('/', (req, res) => res.send(`<a href="http://localhost:5000/Token?RedirectUrl=http%3A%2F%2Flocalhost%3A3000%2Fcallback&Key=${key}"> Get Token</a>`))

app.get('/callback', async (req, res) =>
{
    var token = req.query.token; 
    console.log(token);

    try
    {
        result = await axios.post("http://localhost:5000/Token", {
            key: key,
            tempAuthToken: token
        }, HEADERS);

        console.log(result.data);
        res.send(result.data);
    }
    catch(error)
    {
        console.log(error);
        res.send(error);
    }
});

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
