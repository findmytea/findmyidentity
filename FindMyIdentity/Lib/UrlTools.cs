﻿using System;
using System.Text.RegularExpressions;

namespace FindMyIdentity.Lib
{
    public static class UrlTools
    {
        public static string ExtractQueryParam(string path, string paramName)
        {
            if (paramName is null)
            {
                throw new ArgumentNullException(nameof(paramName));
            }

            var regex = new Regex(paramName + @"=([^&]*)");
            Match match = regex.Match(path);
            return match.Success ? match.Value.Substring(paramName.Length + 1) : null;
        }
    }
}
