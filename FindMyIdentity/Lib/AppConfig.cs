﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyIdentity.Lib
{
    public class AppConfig
    {
        public const string SectionName = "AppConfig";

        public bool DisplayConfirmAccountLink { get; set; }
    }
}
