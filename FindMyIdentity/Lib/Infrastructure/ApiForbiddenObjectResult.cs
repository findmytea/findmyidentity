﻿using System;
using System.Collections;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FindMyIdentity.Lib.Infrastructure
{
    [DefaultStatusCode(DefaultStatusCode)]
    public class ApiForbiddenObjectResult : ObjectResult, IClientErrorActionResult
    {
        private const int DefaultStatusCode = StatusCodes.Status403Forbidden;

        public ApiForbiddenObjectResult(string code, string title, IDictionary errors)
            : base(new ErrorResponse(code, title, errors))
        {
            StatusCode = DefaultStatusCode;
            ErrorResponse = Value as ErrorResponse;
        }

        public ApiForbiddenObjectResult(ModelStateDictionary modelState)
            : base(new ErrorResponse(modelState))
        {
            if (modelState is null)
            {
                throw new ArgumentNullException(nameof(modelState));
            }

            StatusCode = DefaultStatusCode;
            ErrorResponse = Value as ErrorResponse;
        }

        public ErrorResponse ErrorResponse { get; set; }
    }
}
