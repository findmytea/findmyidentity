﻿using System.Collections;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace FindMyIdentity.Lib.Infrastructure
{
    /// <summary>
    /// Error payload
    /// </summary>
    public class ErrorResponse : ResponseBase
    {
        [JsonConstructor]
        public ErrorResponse(IDictionary errors)
        {
            Errors = errors;
        }

        public ErrorResponse(string code = ErrorCodes.Unknown, string title = null, IDictionary errors = null)
        {
            Code = code;
            Title = title ?? "Invalid request.";
            Errors = errors;
        }

        public ErrorResponse(ModelStateDictionary modelState)
        {
            Code = ErrorCodes.BadRequest.Default;
            Title = "One or more validation errors occurred.";
            Errors = new SerializableModelStateErrorDictionary(modelState);
        }

        /// <summary>
        /// Error code
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Code { get; set; }

        /// <summary>
        /// Error title
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        /// <summary>
        /// Error details
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IDictionary Errors { get; }

        /// <summary>
        /// Type
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Status { get; set; }

        /// <summary>
        /// Details
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Detail { get; set; }

        /// <summary>
        /// Instance
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Instance { get; set; }
    }
}
