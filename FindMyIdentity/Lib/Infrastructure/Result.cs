﻿
using System.Collections.Generic;

namespace FindMyIdentity.Lib.Infrastructure
{
    public class Result
    {
        public Result(string code, string title, params KeyValuePair<string, string>[] errors)
        {
            Succeeded = false;
            Code = code;
            Title = title;
            Errors = (errors != null)
                ? new Dictionary<string, string>(errors)
                : new Dictionary<string, string>();
        }

        public Result(string code, string title, IDictionary<string, string> errors = null)
        {
            Succeeded = false;
            Code = code;
            Title = title;
            Errors = errors ?? new Dictionary<string, string>();
        }

        public Result()
        {
            Succeeded = true;
        }

        public bool Succeeded { get; }

        public string Code { get; }

        public string Title { get; }

        public IDictionary<string, string> Errors { get; }

        public static Result<TResult> From<TResult>(TResult result)
            => new Result<TResult>(result);

        public Result WithError(string key, string value)
        {
            Errors.Add(key, value);
            return this;
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:File may only contain a single type", Justification = "Generic verions of the result")]
    public class Result<TResult> : Result
    {
        private Result() { }

        public Result(TResult result)
        {
            Value = result;
        }

        public Result(string code, string title, params KeyValuePair<string, string>[] errors)
            : base(code, title, errors) { }

        public Result(string code, string title, IDictionary<string, string> errors = null)
            : base(code, title, errors) { }

        public TResult Value { get; set; }

        public new Result<TResult> WithError(string key, string value)
            => (Result<TResult>)base.WithError(key, value);
    }
}
