
using System;
using System.Collections;
using Microsoft.AspNetCore.Mvc;

namespace FindMyIdentity.Lib.Infrastructure
{
    public static class ResultExtensions
    {
        public static ActionResult ToActionResult<TResult>(this Result<TResult> result)
        {
            if (result is null)
            {
                throw new ArgumentNullException(nameof(result));
            }
            if (result.Succeeded)
            {
                return new ApiOkObjectResult(result.Value);
            }
            switch (result.Code)
            {
                case ErrorCodes.BadRequest.Default:
                case ErrorCodes.BadRequest.ApiGatewayValidationFailed:
                case ErrorCodes.BadRequest.MobileNumberAlreadyRegistered:
                    return new ApiBadRequestObjectResult(result.Code, result.Title, (IDictionary)result.Errors);
                case ErrorCodes.Forbidden.Default:
                    return new ApiForbiddenObjectResult(result.Code, result.Title, (IDictionary)result.Errors);
                case ErrorCodes.NotFound.Default:
                    return new ApiNotFoundObjectResult(result.Code, result.Title, (IDictionary)result.Errors);
                case ErrorCodes.ServiceUnavailable.Default:
                    return new ApiServiceUnavailableObjectResult(result.Code, result.Title, (IDictionary)result.Errors);
                case ErrorCodes.Unauthorized.Default:
                    return new ApiUnauthorizedObjectResult(result.Code, result.Title, (IDictionary)result.Errors);
                default:
                    return new ApiInternalServerErrorObjectResult(result.Code, result.Title, (IDictionary)result.Errors);
            }
        }
    }
}





