﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace FindMyIdentity.Lib.Infrastructure
{
    [DefaultStatusCode(DefaultStatusCode)]
    public class ApiOkObjectResult : ObjectResult
    {
        private const int DefaultStatusCode = StatusCodes.Status200OK;
        public ApiOkObjectResult(object value)
            : base(value)
        {
            StatusCode = DefaultStatusCode;
        }
    }
}
