﻿
namespace FindMyIdentity.Lib.Infrastructure
{
    public static class ErrorCodes
    {
        public const string Unknown = "0";

        public static class BadRequest
        {
            public const string Default = "400.0";
            public const string ApiGatewayValidationFailed = "400.1";
            public const string MobileNumberAlreadyRegistered = "400.2";
        }

        public static class Unauthorized
        {
            public const string Default = "401.0";
        }

        public static class Forbidden
        {
            public const string Default = "403.0";
        }

        public static class NotFound
        {
            public const string Default = "404.0";
        }

        public static class InternalServerError
        {
            public const string Default = "500.0";
        }

        public static class ServiceUnavailable
        {
            public const string Default = "503.0";
        }
    }
}
