﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FindMyIdentity.Lib.Infrastructure
{
    [Serializable]
    public class SerializableModelStateErrorDictionary : Dictionary<string, object>
    {
        public SerializableModelStateErrorDictionary()
            : base(StringComparer.OrdinalIgnoreCase)
        {
        }

        protected SerializableModelStateErrorDictionary(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public SerializableModelStateErrorDictionary(ModelStateDictionary modelState)
            : this()
        {
            if (modelState is null)
            {
                throw new ArgumentNullException(nameof(modelState));
            }

            if (modelState.IsValid)
            {
                return;
            }

            foreach (var keyModelStatePair in modelState)
            {
                var key = keyModelStatePair.Key;
                var errors = keyModelStatePair.Value.Errors;
                if (errors?.Count > 0)
                {
                    var errorMessages = errors
                        .Select(error => string.IsNullOrEmpty(error.ErrorMessage) ? "The input was not valid." : error.ErrorMessage)
                        .ToArray();
                    Add(key, errorMessages);
                }
            }
        }
    }
}
