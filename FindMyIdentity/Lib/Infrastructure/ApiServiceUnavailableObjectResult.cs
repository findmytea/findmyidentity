﻿using System;
using System.Collections;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FindMyIdentity.Lib.Infrastructure
{
    [DefaultStatusCode(DefaultStatusCode)]
    public class ApiServiceUnavailableObjectResult : ObjectResult, IClientErrorActionResult
    {
        private const int DefaultStatusCode = StatusCodes.Status503ServiceUnavailable;

        public ApiServiceUnavailableObjectResult(string code, string title, IDictionary errors)
            : base(new ErrorResponse(code, title, errors))
        {
            StatusCode = DefaultStatusCode;
            ErrorResponse = Value as ErrorResponse;
        }

        public ApiServiceUnavailableObjectResult(ModelStateDictionary modelState)
            : base(new ErrorResponse(modelState))
        {
            if (modelState is null)
            {
                throw new ArgumentNullException(nameof(modelState));
            }

            StatusCode = DefaultStatusCode;
            ErrorResponse = Value as ErrorResponse;
        }

        public ErrorResponse ErrorResponse { get; set; }
    }
}
