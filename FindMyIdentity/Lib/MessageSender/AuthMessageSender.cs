﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace FindMyIdentity.Lib.MessageSender
{
    public class AuthMessageSender : IEmailSender
    {
        private readonly SmtpCredentials _credentials;
        public AuthMessageSender(SmtpCredentials credentials)
        {
            _credentials = credentials;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            using SmtpClient smtp = new SmtpClient(_credentials.Server);
            smtp.EnableSsl = _credentials.EnableSsl;
            smtp.Port = _credentials.Port;
            smtp.Credentials = new NetworkCredential(_credentials.Username, _credentials.Password);

            using MailMessage message = new MailMessage();
            message.Sender = new MailAddress(_credentials.FromEmail, _credentials.FromName);
            message.From = new MailAddress(_credentials.FromEmail, _credentials.FromName);
            message.To.Add(new MailAddress(email));
            message.Subject = subject;
            message.Body = htmlMessage;
            message.IsBodyHtml = true;
            smtp.Send(message);
            return Task.FromResult(0);
        }
    }
}
