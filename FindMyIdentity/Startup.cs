using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Microsoft.EntityFrameworkCore;
using StackExchange.Redis;
using Microsoft.AspNetCore.DataProtection;
using FindMyIdentity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;

using FindMyIdentity.Lib.MessageSender;
using FindMyIdentity.Lib;


namespace FindMyIdentity
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            Configuration = configuration;
            CurrentEnvironment = env;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        private IWebHostEnvironment CurrentEnvironment { get; set; }

        private readonly ILogger<Startup> _logger;

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureCors(services, Configuration);
            
            try
            {
                var redisKey = CurrentEnvironment.IsDevelopment() ? "FMI-DataProtection-Keys-Dev" : "FMI-DataProtection-Keys";
                services.AddDataProtection().PersistKeysToStackExchangeRedis(ConnectionMultiplexer.Connect(Configuration.GetConnectionString("RedisConnection")), redisKey);
            }
            catch(RedisConnectionException e)
            {
               _logger.LogWarning($"Failed to PersistKeysToStackExchangeRedis: {e.Message}");
            }

            var appConfig = new AppConfig();
            Configuration.GetSection(AppConfig.SectionName).Bind(appConfig);
            services.AddSingleton(appConfig);

            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true).AddEntityFrameworkStores<ApplicationDbContext>();

            var smtpCredentials = new SmtpCredentials();
            Configuration.GetSection("Smtp").Bind(smtpCredentials);
            services.AddTransient<IEmailSender>(s => new AuthMessageSender(smtpCredentials));           
            services.AddRazorPages();

            services.AddAuthentication().AddTwitter(twitterOptions =>
            {
                twitterOptions.ConsumerKey = Configuration["Authentication:Twitter:ConsumerAPIKey"];
                twitterOptions.ConsumerSecret = Configuration["Authentication:Twitter:ConsumerSecret"];
                twitterOptions.RetrieveUserDetails = true;
            })
            .AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            })
           .AddGoogle(options =>
            {
                IConfigurationSection googleAuthNSection = Configuration.GetSection("Authentication:Google");
                options.ClientId = googleAuthNSection["ClientId"];
                options.ClientSecret = googleAuthNSection["ClientSecret"];
            });
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext dbContext)
        {
            try
            {
                dbContext.Database.OpenConnection();
                dbContext.Database.Migrate();
            }
            catch(InvalidOperationException e)
            {
                _logger.LogWarning($"Failed to connect to database: {e.Message}");
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

           app.UseHttpsRedirection();
           app.UseStaticFiles();

            app.UseRouting();
            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                
                endpoints.MapRazorPages();
            });
        }

        private static void ConfigureCors(IServiceCollection services, IConfiguration config)
        {
            var cors = new string[] { "http://localhost:3000", "https://findmytea.herokuapp.com" };
            services.AddCors(options => options.AddDefaultPolicy(builder => builder.WithOrigins(cors).AllowAnyHeader().AllowAnyMethod().AllowCredentials()));
        }
    }
}
