using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore;
using Serilog;
using Serilog.Events;
using System;
using System.Collections;
using System.Net;

namespace FindMyIdentity
{
    public static class Program
    {
        public static int Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .MinimumLevel.Override("Microsoft", LogEventLevel.Verbose)
               .Enrich.FromLogContext()
               .WriteTo.Console()
               .CreateLogger();

            try
            {
                foreach (DictionaryEntry de in Environment.GetEnvironmentVariables())
                {
                    Log.Logger.Information(de.Key + ": " + de.Value);
                }

                BuildWebHost(args).Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Logger.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            var builder = WebHost.CreateDefaultBuilder(args)
                    .ConfigureAppConfiguration((hostingContext, config) =>
                    {
                    })
                    .UseStartup<Startup>()
                    .UseSerilog()
                    .UseUrls("http://*:5010", "https://*:5011");

            return builder.Build();
        }
    }
}
