using System.ComponentModel.DataAnnotations;

namespace FindMyIdentity.Messages
{
    public class DeleteToken
    {
        [Required]
        public string Key { get; set; }

        [Required]
        public string RedirectUrl { get; set; }
    }
}
