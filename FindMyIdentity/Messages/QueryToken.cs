
using System;
using System.ComponentModel.DataAnnotations;

namespace FindMyIdentity.Messages
{
    public class QueryToken
    {
        [Required]
        public Guid Key { get; set; }

        [Required]
        public Uri RedirectUrl { get; set; }
    }
}
