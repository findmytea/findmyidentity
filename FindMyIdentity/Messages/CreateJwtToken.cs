﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FindMyIdentity.Messages
{
    public class CreateJwtToken
    {
        [Required]
        public Guid Key { get; set; }

        [Required]
        public Guid IdToken { get; set; }
    }
}
