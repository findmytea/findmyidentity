
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FindMyIdentity.Lib.Infrastructure;
using FindMyIdentity.Messages;
using FindMyIdentityLib;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FindMyIdentity.Controllers
{
    [ApiController]
    [Route("Token")]   
    public class LocationSearchController : Controller
    {
        private const string IdTokenClaimType = "idtoken";
        
        private readonly SignInManager<IdentityUser> _signInManager;

        private readonly UserManager<IdentityUser> _userManager;

        private readonly ILogger<LocationSearchController> _logger;

        private readonly JsonSerializerSettings _serializerSettings;

        private readonly AuthToken _authToken;

        public LocationSearchController(IConfiguration configuration, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, ILogger<LocationSearchController> logger)
        {
            if (configuration is null)
                throw new ArgumentNullException(nameof(configuration));

            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;

            var auths = new TokenAuthentication();
            configuration.GetSection("TokenAuthentication").Bind(auths);
            _authToken = new AuthToken(auths);

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpGet("IdToken")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status307TemporaryRedirect)]
        public async Task<ActionResult> QueryIdToken([FromQuery] QueryToken query)
        {
            _logger.LogInformation("QueryToken");
            
            if (query is null)
                throw new ArgumentNullException(nameof(query));

            if (!_authToken.ValdateKey(query.Key))
                return NotFound();

            var url = query.RedirectUrl.ToString();
            var token = _authToken.GenerateToken();

            var user = await _userManager.GetUserAsync(HttpContext.User);
            await ClearIdTokens(user);
            await _userManager.AddClaimAsync(user, new Claim(IdTokenClaimType, token.ToString()));
            return Redirect($"{url}?token={token}");
        }       

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        public async Task<IActionResult> CreateJwtToken([FromBody] CreateJwtToken query)
        {
            _logger.LogInformation("CreateJwtToken");

            if (query is null)
                throw new ArgumentNullException(nameof(query));

            if (!_authToken.ValdateKey(query.Key))
                return NotFound();

            var users = await _userManager.GetUsersForClaimAsync(new Claim(IdTokenClaimType, query.IdToken.ToString()));
            if (users.Count != 1)
                return NotFound();

            var user = users.FirstOrDefault();
            var token = _authToken.Generate(user.Email, await _userManager.GetClaimsAsync(user));

            var result = Result.From(token).ToActionResult();
            return result;
        }

        [HttpGet("Logout")]
        [ProducesResponseType(StatusCodes.Status307TemporaryRedirect)]
        public async Task<ActionResult> DeleteTokenAsync([FromQuery] DeleteToken command)
        {
            _logger.LogInformation("DeleteTokenAsync");

            if (command is null)
                throw new ArgumentNullException(nameof(command));

            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user != null)
            { 
                await ClearIdTokens(user);
            }

            await _signInManager.SignOutAsync();
            var url = command.RedirectUrl.ToString();
            return Redirect($"{url}");
        }

        private async Task ClearIdTokens(IdentityUser user)
        {
            var claimsToRemove = (await _userManager.GetClaimsAsync(user)).Where(c => c.Type == IdTokenClaimType);
            await _userManager.RemoveClaimsAsync(user, claimsToRemove);
        }
    }
}