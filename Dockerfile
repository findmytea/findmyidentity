FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

ARG SOURCE

# Copy everything else and build
COPY . ./
RUN dotnet restore --source $SOURCE --no-cache
RUN dotnet publish FindMyIdentity.sln -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 
WORKDIR /app
COPY --from=build-env /app/out .

EXPOSE 5011
ENTRYPOINT [ "dotnet", "FindMyIdentity.dll" ]

